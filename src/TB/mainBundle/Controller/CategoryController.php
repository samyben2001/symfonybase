<?php

namespace TB\mainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TB\mainBundle\Entity\Category;
use TB\mainBundle\Form\CategoryType;


class CategoryController extends Controller
{
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository("MainBundle:Category");
        $categories = $repo->findAll();
        return $this->render("MainBundle:Category:index.html.twig", array("categories" => $categories));
    }

function createAction(Request $request)
    {
        $cat = new Category();
        $form = $this->createForm(CategoryType::class, $cat);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cat);
            $em->flush();
            $this->addFlash("success", "Catégorie " . $cat->getCategoryName() . " ajoutée!");
            return $this->redirectToRoute("main_category_index");
        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute("main_home_error");
        }else{
            return $this->render("MainBundle:Category:create.html.twig", array("form" => $form->createView()));
        }
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function updateAction(Category $cat, Request $request)
    {
        $form = $this->createForm(CategoryType::class, $cat);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $this->addFlash("success", "Catégorie " . $cat->getCategoryName() . " mise à jour!");
            $em->persist($cat);
            $em->flush();
            return $this->redirectToRoute("main_category_index");
        }
        return $this->render("MainBundle:Category:update.html.twig", array("form" => $form->createView()));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function deleteAction(Category $cat)
    {
        $em = $this->getDoctrine()->getManager();
        $this->addFlash("error", "Catégorie " . $cat->getCategoryName() . " supprimée!");
        $em->remove($cat);
        $em->flush();
        return $this->redirectToRoute('main_category_index');
    }


}
