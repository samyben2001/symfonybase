<?php

namespace TB\mainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use TB\mainBundle\Entity\User;
use TB\mainBundle\Form\UserType;
use TB\mainBundle\Utils\FileService;

class UserController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    public function loginAction()
    {
        return $this->render("MainBundle:User:login.html.twig");
    }

        public function registerAction(Request $request, FileService $fileService, UserPasswordEncoderInterface $encoder)
    {
        $u = new User();

        $form = $this->createForm(UserType::class, $u);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

//            $u->setUserSalt(uniqid());
//            $pwd = $u->getUserPassword() . $u->getUserSalt();
//            $cryptePwd = hash('sha512', $pwd);
//            $u->setUserPassword($cryptePwd);

            $u->setUserRoles(["ROLE_USER"]);
            $u->setUserSalt(uniqid());
            $encoded = $encoder->encodePassword($u, $u->getUserPassword());
            $u->setUserPassword($encoded);

            $em->persist($u);

            $pic = $u->getUserPhoto();
            $url = $fileService->moveuploadedFile($pic->getFile());
            $pic->setPictureURL($url);

            $em->persist($pic);
            $em->flush();
            $this->addFlash("success", "Utilisateur " . $u->getUserLastName() . " ajouté!");
        }


        return $this->render("MainBundle:User:register.html.twig", array("form" => $form->createView()));
    }

    /**
     * @Security("has_role('ROLE_USER)")
     *
     */
    public function updateAction(User $u, Request $request, FileService $fileService)
    {
        $form = $this->createForm(UserType::class, $u);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($u);

            $u->setUserSalt(uniqid());
            $pwd = $u->getUserPassword() . $u->getUserSalt();
            $cryptePwd = hash('sha512', $pwd);
            $u->setUserPassword($cryptePwd);
            $u->setUserRoles(["ROLE_USER"]);

            $pic = $u->getUserPhoto();
            $url = $fileService->moveuploadedFile($pic->getFile());
            $pic->setPictureURL($url);

            $this->addFlash("success", "Votre profil à été mis à jour!");
            $em->persist($pic);
            $em->flush();

            return $this->redirectToRoute("main_user_update");
        }


        return $this->render("MainBundle:User:profil.html.twig", array("form" => $form->createView()));
    }
}
