<?php

namespace TB\mainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TB\mainBundle\Entity\Game;
use TB\mainBundle\Form\GameType;
use TB\mainBundle\Utils\FileService;

class GameController extends Controller
{
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository("MainBundle:Game");
        $games = $repo->findAll();


        return $this->render("MainBundle:Game:index.html.twig", array("games" => $games));
    }

    public function detailsAction($id)
    {
        $repo = $this->getDoctrine()->getRepository("MainBundle:Game");
        $details = $repo->getGameWithPicturesWithPlatforms($id);
//        dump($details);die;
        return $this->render("MainBundle:Game:details.html.twig", array("details" => $details));
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function createAction(Request $request, FileService $fileService)
    {
        $g = new Game();

        $form = $this->createForm(GameType::class, $g);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($g);
            foreach ($g->getGamePlatforms() as $pf) {
                $pf->setGamePlatformGame($g);
                $em->persist($pf);
            }
            foreach ($g->getGamePictures() as $pic) {
                $url = $fileService->moveuploadedFile($pic->getFile());
                $pic->setPictureURL($url);
                $pic->setPictureGame($g);
                $em->persist($pic);
            }
            $em->flush();
            $this->addFlash("success", "Jeu " . $g->getGameName() . " ajouté!");
            return $this->redirectToRoute("main_game_index");
        }

        return $this->render(
            "MainBundle:Game:create.html.twig", array("form" => $form->createView()));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function updateAction(Game $g, Request $request, FileService $fileService)
    {
        $form = $this->createForm(GameType::class, $g);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($g);
            foreach ($g->getGamePlatforms() as $pf) {
                $pf->setGamePlatformGame($g);
                $em->persist($pf);
            }
            
            foreach ($g->getGamePictures() as $pic) {
                $url = $fileService->moveuploadedFile($pic->getFile());
                $pic->setPictureURL($url);
                $pic->setPictureGame($g);
                $em->persist($pic);
            }
            $em->flush();
            $this->addFlash("success", "Jeu " . $g->getGameName() . " mis à jour!");
            return $this->redirectToRoute("main_game_index");
        }

        return $this->render(
            "MainBundle:Game:update.html.twig", array("form" => $form->createView()));
    }


/**
 * @Security("has_role('ROLE_ADMIN')")
 *
 */
public
function deleteAction()
{

}
}
