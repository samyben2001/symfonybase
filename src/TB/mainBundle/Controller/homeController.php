<?php

namespace TB\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use TB\mainBundle\Entity\User;
use TB\mainBundle\Utils\Mailer;

class homeController extends Controller
{
    public function homeAction()
    {
        return $this->render('MainBundle:home:home.html.twig', array());
    }

    public function sendEmailAction(\Symfony\Component\HttpFoundation\Request $request, Mailer $mailer)
    {
        $nom = $request->request->get('name');
        $email = $request->request->get('email');
        $sujet = $request->request->get('subject');
        $message = $request->request->get('message');
        $mailer->sendEmail($sujet, $this->renderView('MainBundle:home:mail.html.twig', array('nom' => $nom, 'email' => $email, 'message' => $message, 'sujet' => $sujet)));
        return $this->redirectToRoute('main_home_home');

    }

    public function aboutUsAction()
    {
        return $this->render('MainBundle:home:aboutUs.html.twig', array());
    }

    public function contactAction()
    {
        return $this->render('MainBundle:home:contact.html.twig', array());
    }

    public function errorAction()
    {
        return $this->render('MainBundle:home:error.html.twig', array());
    }


}
