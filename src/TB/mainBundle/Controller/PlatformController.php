<?php

namespace TB\mainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TB\mainBundle\Entity\Platform;
use TB\mainBundle\Form\PlatformType;

class PlatformController extends Controller
{
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository("MainBundle:Platform");
        $platforms = $repo->findAll();
        return $this->render("MainBundle:Platform:index.html.twig", array("platforms" => $platforms));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function createAction(Request $request)
    {
        $pf = new Platform();
        $form = $this->createForm(PlatformType::class, $pf);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($pf);
            $em->flush();
            $this->addFlash("success", "Plateforme ".$pf->getPlatformName()." ajoutée!");
            return $this->redirectToRoute("main_platform_index");
        }
        return $this->render("MainBundle:Platform:create.html.twig", array("form" => $form->createView()));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function updateAction(Platform $pf, Request $request)
    {
        $form = $this->createForm(PlatformType::class, $pf);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($pf);
            $em->flush();
            $this->addFlash("success", "Plateforme ".$pf->getPlatformName()." modifiée!");
            return $this->redirectToRoute("main_platform_index");
        }
        return $this->render("MainBundle:Platform:update.html.twig", array("form" => $form->createView()));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function deleteAction(Platform $pf)
    {
        $em = $this->getDoctrine()->getManager();
        $this->addFlash("error", "Plateforme ".$pf->getPlatformName()." supprimée!");
        $em->remove($pf);
        $em->flush();
        return $this->redirectToRoute('main_platform_index');
    }
}
