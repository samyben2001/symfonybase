<?php

namespace TB\mainBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GameType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gameName', TextType::class, array(
                'label' => "Nom du jeu",
                'required' => true))
            ->add('gameCategory', EntityType::class, array(
                'class' => 'TB\mainBundle\Entity\Category',
                'choice_label' => 'categoryName',
                'expanded' => false,
                'multiple' => false,
                'label' => "Catégorie"))
            ->add('gamePlatforms', CollectionType::class, array(
                'allow_add' => true,
                'entry_type' => Game_PlatformType::class,
                'label' => false))
            ->add('gamePictures', CollectionType::class, array(
                'allow_add' => true,
                'entry_type' => PictureType::class,
                'label' => false))
            ->add('submit', SubmitType::class, array('label' => "Enregistrer"));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TB\mainBundle\Entity\Game'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tb_mainbundle_game';
    }


}
