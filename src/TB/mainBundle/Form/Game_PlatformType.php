<?php

namespace TB\mainBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Game_PlatformType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gamePlatformPlatform', EntityType::class, array(
                'class' => 'TB\mainBundle\Entity\Platform',
                'choice_label' => 'platformName',
                'expanded' => false,
                'multiple' => false,
                'label' => false))
            ->add('gamePlatformReleaseDate', DateType::class, array('label' => "Date de sortie"))
            ->add('gamePlatformPrice', MoneyType::class, array('label' => 'Prix'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TB\mainBundle\Entity\Game_Platform'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tb_mainbundle_game_platform';
    }


}
