<?php

namespace TB\mainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="TB\mainBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="GameId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $gameId;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="TB\mainBundle\Entity\Category")
     * @ORM\JoinColumn(referencedColumnName="CategoryId")
     */
    private $gameCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="GameName", type="string", length=255)
     * @Asserts\Length(min=2,minMessage="Le nom du jeu doit être au minimum de 2 caractères",
     *     max="25", maxMessage="Le nom du jeu doit être au maximum de 25 caractères")
     */
    private $gameName;

    /**
     * @var Picture[]
     * @ORM\OneToMany(targetEntity="TB\mainBundle\Entity\Picture", mappedBy="pictureGame")
     * @ORM\JoinColumn(referencedColumnName="PictureId")
     * @Asserts\Valid()
     */
    private $gamePictures;


    /**
     * @var Game_Platform[]
     * @ORM\OneToMany(targetEntity="TB\mainBundle\Entity\Game_Platform", mappedBy="gamePlatformGame")
     */
    private $gamePlatforms;

    /**
     * Get id
     *
     * @return int
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set gameName
     *
     * @param string $gameName
     *
     * @return Game
     */
    public function setGameName($gameName)
    {
        $this->gameName = $gameName;

        return $this;
    }

    /**
     * Get gameName
     *
     * @return string
     */
    public function getGameName()
    {
        return $this->gameName;
    }

    /**
     * @return Category
     */
    public function getGameCategory()
    {
        return $this->gameCategory;
    }

    /**
     * @param Category $gameCategory
     * @return Game
     */
    public function setGameCategory($gameCategory)
    {
        $this->gameCategory = $gameCategory;
        return $this;
    }

    /**
     * @return Picture[]
     */
    public function getGamePictures()
    {
        return $this->gamePictures;
    }

    /**
     * @param Picture[] $gamePictures
     *
     * @return Game
     */
    public function setGamePictures($gamePictures)
    {
        $this->gamePictures = $gamePictures;
        return $this;
    }

    /**
     * @return Game_Platform[]
     */
    public function getGamePlatforms()
    {
        return $this->gamePlatforms;
    }

    /**
     * @param Game_Platform[] $gamePlatforms
     * @return Game
     */
    public function setGamePlatforms($gamePlatforms)
    {
        $this->gamePlatforms = $gamePlatforms;
        return $this;
    }

    /**
     * @param Game_Platform[] $gamePlatform
     */
    public function addGamePlatform($gamePlatform)
    {
        $this->gamePlatforms = $gamePlatform;
        return $this;
    }

    public function __construct()
    {
        $this->gamePlatforms = [];
    }



}

