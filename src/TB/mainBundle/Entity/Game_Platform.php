<?php

namespace TB\mainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game_Platform
 *
 * @ORM\Table(name="game__platform")
 * @ORM\Entity(repositoryClass="TB\mainBundle\Repository\Game_PlatformRepository")
 */
class Game_Platform
{
    /**
     * @var int
     *
     * @ORM\Column(name="Game_PlatformId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $game_PlatformId;


    /**
     * @var Game
     *
     * @ORM\ManyToOne(targetEntity="TB\mainBundle\Entity\Game", inversedBy="gamePlatforms")
     * @ORM\JoinColumn(referencedColumnName="GameId")
     */
    private $gamePlatformGame;

    /**
     * @var Platform
     *
     * @ORM\ManyToOne(targetEntity="TB\mainBundle\Entity\Platform")
     * @ORM\JoinColumn(referencedColumnName="PlatformId")
     */
    private $gamePlatformPlatform;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="GamePlatformReleaseDate", type="date", nullable=true)
     */
    private $gamePlatformReleaseDate;

    /**
     * @var float
     *
     * @ORM\Column(name="GamePlatformPrice", type="float", nullable=true)
     */
    private $gamePlatformPrice;


    /**
     * Get id
     *
     * @return int
     */
    public function getGamePlatformId()
    {
        return $this->game_PlatformId;
    }

    /**
     * Set gamePlatformReleaseDate
     *
     * @param \DateTime $gamePlatformReleaseDate
     *
     * @return Game_Platform
     */
    public function setGamePlatformReleaseDate($gamePlatformReleaseDate)
    {
        $this->gamePlatformReleaseDate = $gamePlatformReleaseDate;

        return $this;
    }

    /**
     * Get gamePlatformReleaseDate
     *
     * @return \DateTime
     */
    public function getGamePlatformReleaseDate()
    {
        return $this->gamePlatformReleaseDate;
    }

    /**
     * Set gamePlatformPrice
     *
     * @param float $gamePlatformPrice
     *
     * @return Game_Platform
     */
    public function setGamePlatformPrice($gamePlatformPrice)
    {
        $this->gamePlatformPrice = $gamePlatformPrice;

        return $this;
    }

    /**
     * Get gamePlatformPrice
     *
     * @return float
     */
    public function getGamePlatformPrice()
    {
        return $this->gamePlatformPrice;
    }

    /**
     * @return Game
     */
    public function getGamePlatformGame()
    {
        return $this->gamePlatformGame;
    }

    /**
     * @param Game $gamePlatformGame
     * @return Game_Platform
     */
    public function setGamePlatformGame($gamePlatformGame)
    {
        $this->gamePlatformGame = $gamePlatformGame;
        return $this;
    }

    /**
     * @return Platform
     */
    public function getGamePlatformPlatform()
    {
        return $this->gamePlatformPlatform;
    }

    /**
     * @param Platform $gamePlatformPlatform
     * @return  Game_Platform
     */
    public function setGamePlatformPlatform($gamePlatformPlatform)
    {
        $this->gamePlatformPlatform = $gamePlatformPlatform;
        return $this;
    }
}

