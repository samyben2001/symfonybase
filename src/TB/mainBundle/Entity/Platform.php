<?php

namespace TB\mainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Platform
 *
 * @ORM\Table(name="platform")
 * @ORM\Entity(repositoryClass="TB\mainBundle\Repository\PlatformRepository")
 */
class Platform
{
    /**
     * @var int
     *
     * @ORM\Column(name="PlatformId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $platformId;

    /**
     * @var string
     *
     * @ORM\Column(name="PlatformName", type="string", length=255)
     */
    private $platformName;


    /**
     * Get id
     *
     * @return int
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * Set platformName
     *
     * @param string $platformName
     *
     * @return Platform
     */
    public function setPlatformName($platformName)
    {
        $this->platformName = $platformName;

        return $this;
    }

    /**
     * Get platformName
     *
     * @return string
     */
    public function getPlatformName()
    {
        return $this->platformName;
    }
}

