<?php

namespace TB\mainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Asserts;

/**
 * Picture
 *
 * @ORM\Table(name="picture")
 * @ORM\Entity(repositoryClass="TB\mainBundle\Repository\PictureRepository")
 */
class Picture
{
    /**
     * @var int
     *
     * @ORM\Column(name="PictureId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $PictureId;

    /**
     * @var Game
     *
     * @ORM\ManyToOne(targetEntity="TB\mainBundle\Entity\Game", inversedBy="gamePictures")
     * @ORM\JoinColumn(nullable=true,referencedColumnName="GameId")
     *
     */
    private $pictureGame;

    /**
     * @var string
     *
     * @ORM\Column(name="PictureURL", type="string", length=255)
     */
    private $pictureURL;

    /**
     * @var mixed
     * @Asserts\File(mimeTypes={"image/jpeg", "image/png"}, mimeTypesMessage="Format non valide",
     *     maxSize="1000000", maxSizeMessage="La taille du fichier dépasse la limite(10mo)")
     *
     */
    private $file;


    /**
     * Get id
     *
     * @return int
     */
    public function getPictureId()
    {
        return $this->PictureId;
    }

    /**
     * Set pictureURL
     *
     * @param string $pictureURL
     *
     * @return Picture
     */
    public function setPictureURL($pictureURL)
    {
        $this->pictureURL = $pictureURL;

        return $this;
    }

    /**
     * Get pictureURL
     *
     * @return string
     */
    public function getPictureURL()
    {
        return $this->pictureURL;
    }

    /**
     * @return Game
     */
    public function getPictureGame()
    {
        return $this->pictureGame;
    }

    /**
     * @param Game $pictureGame
     * @return Picture
     */
    public function setPictureGame($pictureGame)
    {
        $this->pictureGame = $pictureGame;
        return $this;
    }

    /**
     * @param mixed $file
     * @return Picture
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }
}

