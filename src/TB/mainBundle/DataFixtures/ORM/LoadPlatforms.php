<?php
/**
 * Created by PhpStorm.
 * User: Student
 * Date: 04-09-17
 * Time: 13:29
 */

namespace TB\mainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TB\mainBundle\Entity\Platform;

class LoadPlatforms implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $pfList = ["Playstation 3","Playstation 4","Nintendo Switch","Xbox One","Xbox 360","PC","Mac","Nintendo 3DS"];

        foreach ($pfList as $pf){
            $p = new Platform();
            $p->setPlatformName($pf);
            $manager->persist($p);
        }
        $manager->flush();
    }
}