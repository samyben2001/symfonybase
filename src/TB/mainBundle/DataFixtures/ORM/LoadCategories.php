<?php
/**
 * Created by PhpStorm.
 * User: Student
 * Date: 04-09-17
 * Time: 11:38
 */

namespace TB\mainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TB\mainBundle\Entity\Category;

class LoadCategories implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $catList = ["Action","Aventure","Simulation","FPS","RPG","Plate-forme","Sport","Combat"];

        foreach ($catList as $cat){
            $c = new Category();
            $c->setCategoryName($cat);
            $manager->persist($c);
        }
        $manager->flush();
    }
}