<?php
/**
 * Created by PhpStorm.
 * User: Student
 * Date: 04-09-17
 * Time: 11:56
 */

namespace TB\mainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TB\mainBundle\Entity\Picture;
use TB\mainBundle\Entity\User;

class LoadUsers implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $imgAdmin = new Picture();
        $imgAdmin->setPictureURL("assets/img/profile/default.png");
        $admin = new User();
        $admin
            ->setUserFirstName("Samy")
            ->setUserLastName("Ben Beya")
            ->setUserRoles(["ROLE_ADMIN"])
            ->setUserEmail("amdin@admin.com")
            ->setUserPassword("admin")
            ->setUserSalt(uniqid())
            ->setUserPhoto($imgAdmin);

        $imgCustomer = new Picture();
        $imgCustomer->setPictureURL("assets/img/profile/default.png");
        $customer = new User();
        $customer
            ->setUserFirstName("Khun")
            ->setUserLastName("Ly")
            ->setUserRoles(["ROLE_USER"])
            ->setUserEmail("customer@customer.com")
            ->setUserPassword("user")
            ->setUserSalt(uniqid())
            ->setUserPhoto($imgCustomer);

        $manager->persist($admin);
        $manager->persist($customer);
        $manager->flush();
    }
}