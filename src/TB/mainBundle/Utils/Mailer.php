<?php
/**
 * Created by PhpStorm.
 * User: Student
 * Date: 01-09-17
 * Time: 09:44
 */

namespace TB\mainBundle\Utils;


class Mailer
{
    private $mailer_host;
    private $mailer_user;
    private $mailer_password;
    private $swift_mailer;

    public function __construct($mailer_host, $mailer_user, $mailer_password, \Swift_Mailer $swift_Mailer )
    {
        $this->mailer_host = $mailer_host;
        $this->mailer_user = $mailer_user;
        $this->mailer_password = $mailer_password;
        $this->swift_mailer = $swift_Mailer;

    }

    public function sendEmail($sujet,$contenu)
    {
        $message = \Swift_Message::newInstance();
        $message->setSubject($sujet);
        $message->setFrom($this->mailer_user);
        $message->setBody($contenu, 'text/html');
        $message->setTo($this->mailer_user);
        $this->swift_mailer->send($message);
    }

}